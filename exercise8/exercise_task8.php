<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD Example</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h2 {
            color: #333;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 10px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        .forms-container {
            display: flex;
            justify-content: space-between;
            margin-top: 20px;
        }

        form {
            max-width: 300px;
            padding: 20px;
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
            border-radius: 8px;
        }

        label {
            margin-bottom: 5px;
            font-weight: bold;
        }

        input {
            margin-bottom: 10px;
            padding: 8px;
            box-sizing: border-box;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 10px;
            border: none;
            cursor: pointer;
            margin-right: 5px;
            border-radius: 5px;
        }

        button:hover {
            background-color: #45a049;
        }

        .cancel-button {
            background-color: #ccc;
            color: #333;
        }

        .cancel-button:hover {
            background-color: #999;
        }
    </style>
</head>
<body>
<?php

require_once('MysqliDb.php');
require_once('dbObject.php');


$db = new MysqliDb('localhost', 'your_username', 'your_password', 'p8_exercise_backend');
dbObject::autoload("models");


$successMessage = "";
$errorMessage = "";


$searchQuery = isset($_GET['search']) ? $_GET['search'] : '';
$searchCondition = !empty($searchQuery) ? "WHERE first_name LIKE '%$searchQuery%' OR last_name LIKE '%$searchQuery%'" : '';

// Handle form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['create'])) {
        $employee = new employee;
        
        $employee->first_name = trim($_POST['first_name']);
        $employee->last_name = trim($_POST['last_name']);
        $employee->middle_name = trim($_POST['middle_name']);
        $employee->birthday = trim($_POST['birthday']);
        $employee->address = trim($_POST['address']);


        if ($employee->save()) {
            $successMessage = "New record created successfully";
            header("Location: {$_SERVER['PHP_SELF']}");
            exit();
        } else {
            $errorMessage = "Error: " . $employee->getError();
        }
    } elseif (isset($_POST['update'])) {
        $id = $_POST['id'];
        $employee = employee::byId($id);

        $employee->first_name = trim($_POST['first_name']);
        $employee->last_name = trim($_POST['last_name']);
        $employee->middle_name = trim($_POST['middle_name']);
        $employee->birthday = $_POST['birthday'];
        $employee->address = trim($_POST['address']);

        if (!empty($id) && !empty($employee->first_name) && !empty($employee->last_name)) {
            if ($employee->save()) {
                $successMessage = "Record updated successfully";
                header("Location: {$_SERVER['PHP_SELF']}");
                exit();
            } else {
                $errorMessage = "Error updating record: " . $employee->getLastError();
            }
        } else {
            $errorMessage = "Please enter a valid ID, first name, and last name";
        }
    } elseif (isset($_POST['delete'])) {
        $id = $_POST['id'];
        $employee = employee::byId($id);
        
        if (!empty($id)) {
            if ($employee && $employee->delete()) {
                $successMessage = "Record deleted successfully";
                header("Location: {$_SERVER['PHP_SELF']}");
                exit();
            } else {
                $errorMessage = $employee ? "Error deleting record" : "Record not found";
            }
        } else {
            $errorMessage = "Please enter a valid ID";
        }
    } elseif (isset($_POST['edit'])) {
        $edit_id = $_POST['edit_id'];
        $employee = employee::byId($edit_id);
       
        $result =  $employee;

        if ($result) {
            $edit_first_name = $employee->first_name;
            $edit_last_name = $employee->last_name;
            $edit_middle_name = $employee->middle_name;
            $edit_birthday = $employee->birthday;
            $edit_address = $employee->address;
        } else {
            $errorMessage = "Error: Record not found.";
        }
    }
}

if (!empty($successMessage)) {
    echo "<p style='color: green;'>$successMessage</p>";
}

if (!empty($errorMessage)) {
    echo "<p style='color: red;'>$errorMessage</p>";
}

$sql = "SELECT * FROM employee $searchCondition";
$result = $db->rawQuery($sql);

echo "<form method='GET' style='margin-bottom: 20px;'>
        <label for='search'>Search:</label>
        <input type='text' id='search' name='search' value='$searchQuery'>
        <button type='submit'>Search</button>
      </form>";

echo "<h2>Read</h2>";
echo "<table border='1'><tr><th>ID</th><th>FirstName</th><th>LastName</th><th>Middle Name</th><th>Birthday</th><th>Address</th>";

if ($result) {
    foreach ($result as $row) {
        echo "<tr>";
        echo "<td>" . $row["id"] . "</td>";
        echo "<td>" . $row["first_name"] . "</td>";
        echo "<td>" . $row["last_name"] . "</td>";
        echo "<td>" . $row["middle_name"] . "</td>";
        echo "<td>" . date('F j, Y', strtotime($row["birthday"])) . "</td>";
        echo "<td>" . $row["address"] . "</td>";
        echo "</tr>";
    }
} else {
    echo "<tr><td colspan='5'>0 results</td></tr>";
}

echo "</table>";


?>


<div class="forms-container">
    <div>
        <h2>Create</h2>
        <form method="POST">
            <div style="margin-bottom: 10px;">
                <label for="first_name">First Name:</label>
                <input type="text" id="first_name" name="first_name" required>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="last_name">Last Name:</label>
                <input type="text" id="last_name" name="last_name" required>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="middle_name">Middle Name:</label>
                <input type="text" id="middle_name" name="middle_name">
            </div>

            <div style="margin-bottom: 10px;">
                <label for="birthday">Birthday:</label>
                <input type="date" id="birthday" name="birthday" required>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="address">Address:</label>
                <input type="text" id="address" name="address">
            </div>

            <div style="display: flex; justify-content: flex-end;">
                <button type="submit" name="create">Submit</button>
                <button type="button" class="cancel-button" onclick="clearCreateForm()">Clear</button>
            </div>
        </form>
    </div>

    <div>
        <h2>Update</h2>
        <form id="updateForm" method="POST" style="display: none;">
            <div style="margin-bottom: 10px;">
                <label for="id">ID:</label>
                <input type="number" id="update_id" name="id" readonly>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="update_first_name">First Name:</label>
                <input type="text" id="update_first_name" name="first_name" required>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="update_last_name">Last Name:</label>
                <input type="text" id="update_last_name" name="last_name" required>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="update_middle_name">Middle Name:</label>
                <input type="text" id="update_middle_name" name="middle_name">
            </div>

            <div style="margin-bottom: 10px;">
                <label for="update_birthday">Birthday:</label>
                <input type="date" id="update_birthday" name="birthday" required>
            </div>

            <div style="margin-bottom: 10px;">
                <label for="update_address">Address:</label>
                <input type="text" id="update_address" name="address">
            </div>

            <div style="display: flex; justify-content: flex-end;">
                <button type="submit" name="update">Update</button>
                <button type="button" class="cancel-button" onclick="clearUpdateForm()">Cancel</button>
            </div>
        </form>

        <form method="POST">
            <label for="edit_id">Enter ID to edit:</label>
            <input type="number" id="edit_id" name="edit_id" required>
            <button type="submit" name="edit">Edit</button>
        </form>
    </div>
    
    <div>
        <h2>Delete</h2>
        <form method="POST">
            <label for="id">Enter ID:</label>
            <input type="number" id="delete_id" name="id" required>
            <button type="submit" name="delete">Delete</button>
        </form>
    </div>
</div>

<script>
    <?php if (isset($edit_first_name) && isset($edit_last_name)) : ?>
        document.getElementById('updateForm').style.display = 'block';
        document.getElementById('update_id').value = <?php echo $edit_id; ?>;
        document.getElementById('update_first_name').value = '<?php echo $edit_first_name; ?>';
        document.getElementById('update_last_name').value = '<?php echo $edit_last_name; ?>';
        document.getElementById('update_middle_name').value = '<?php echo $edit_middle_name; ?>';
        document.getElementById('update_birthday').value = '<?php echo $edit_birthday; ?>';
        document.getElementById('update_address').value = '<?php echo $edit_address; ?>';
    <?php endif; ?>
    
    function clearUpdateForm() {
        document.getElementById('update_id').value = '';
        document.getElementById('update_first_name').value = '';
        document.getElementById('update_last_name').value = '';
        document.getElementById('update_middle_name').value = '';
        document.getElementById('update_birthday').value = '';
        document.getElementById('update_address').value = '';
        document.getElementById('updateForm').style.display = 'none';
    }
    function clearCreateForm() {
        document.getElementById('first_name').value = '';
        document.getElementById('last_name').value = '';
        document.getElementById('middle_name').value = '';
        document.getElementById('birthday').value = '';
        document.getElementById('address').value = '';
    }
</script>

</body>
</html>