<?php
class Employee extends dbObject {
    protected $dbTable = 'employee';
    protected $primaryKey = 'id';
    protected $dbFields = Array (
        'id' => Array ('int'),
        'first_name' => Array ('text'),
        'last_name' => Array ('text'),
        'middle_name' => Array ('text'),
        'birthday' => Array ('text'),
        'address' => Array ('text')
    );
}
?>