<!-- Inside of it, create a class named "API". 
3. Inside of the class, create a 3 function with parameters that execute printing the parameters you pass.
4. Outside of Class, use the class and call the function you created. 
5. In the 1st function, put your full name as the parameter of it.
6. In the 2nd Function, put an Array Value of your hobbies as the parameter of it.
7. In the 3rd Function, put an Object Value of your Age, email address and Birthday as the parameter of it.

Example of Expected Output:
Full Name: Juan Dela Cruz
Hobbies:
     Playing Basketball
     Reading Books
     Listening Music
Age: 20
Email: juandelacruz@emailniya.com
Birthday: January 1, 1987 -->


<?php

class API {

    public function printFullName($name) {
        echo "Full Name: " . $name . "\n";
    }

    public function printHobbies($hobbies) {
        echo "Hobbies:\n";
        foreach ($hobbies as $hobby) {
            echo "    " . $hobby . "\n";
        }
    }

    public function printPersonalInfo($info) {
        echo "Age: " . $info->age . "\n";
        echo "Email: " . $info->email . "\n";
        echo "Birthday: " . $info->birthday . "\n";
    }
}

$api = new API();

$name = "Juan Dela Cruz";
$hobbies = ["Playing Basketball", "Reading Books", "Listening Music"];
$info = (object) ["age" => 20, "email" => "juandelacruz@emailniya.com", "birthday" => "January 1, 1987"];

$api->printFullName($name);
$api->printHobbies($hobbies);
$api->printPersonalInfo($info);

?>