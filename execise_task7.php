<?php
// Connect to the database using MySQLi
$host = 'localhost';
$username = 'your_username';
$password = 'your_password';
$database = 'your_database_name';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Handle form submission
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['create'])) {
        $name = trim($_POST['name']);

        if (!empty($name)) {
            $stmt = $conn->prepare("INSERT INTO employee (name) VALUES (?)");
            $stmt->bind_param("s", $name);

            if ($stmt->execute()) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $stmt->error;
            }

            $stmt->close();
        } else {
            echo "Please enter a name";
        }
    } elseif (isset($_POST['update'])) {
        $id = $_POST['id'];
        $name = trim($_POST['name']);

        if (!empty($id) && !empty($name)) {
            $stmt = $conn->prepare("UPDATE employee SET name=? WHERE id=?");
            $stmt->bind_param("si", $name, $id);

            if ($stmt->execute()) {
                echo "Record updated successfully";
            } else {
                echo "Error updating record: " . $stmt->error;
            }

            $stmt->close();
        } else {
            echo "Please enter a valid ID and name";
        }
    } elseif (isset($_POST['delete'])) {
        $id = $_POST['id'];

        if (!empty($id)) {
            $stmt = $conn->prepare("DELETE FROM employee WHERE id=?");
            $stmt->bind_param("i", $id);

            if ($stmt->execute()) {
                echo "Record deleted successfully";
            } else {
                echo "Error deleting record: " . $stmt->error;
            }

            $stmt->close();
        } else {
            echo "Please enter a valid ID";
        }
    }
}

// Read data from the employee table
$sql = "SELECT * FROM employee";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Display a table of the employee data
    echo "<table><tr><th>ID</th><th>Name</th></tr>";
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["id"]. "</td><td>" . $row["name"]. "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>
    <title>CRUD Example</title>
</head>
<body>

<h2>Create</h2>
<form method="POST">
    <label for="name">Enter name:</label>
    <input type="text" id="name" name="name">
    <button type="submit" name="create">Submit</button>
</form>

<h2>Read</h2>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    <?php
    if ($result->num_rows > 0) {
        // Display a table of the employee data
        while($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["id"]. "</td>";
            echo "<td>" . $row["name"]. "</td>";
            echo "</tr>";
        }
    } else {
        echo "<tr><td colspan='2'>0 results</td></tr>";
    }
    ?>
</table>

<h2>Update</h2>
<form method="POST">
    <label for="id">Enter ID:</label>
    <input type="number" id="id" name="id">
    <label for="name">Enter name:</label>
    <input type="text" id="name" name="name">
    <button type="submit" name="update">Update</button>
</form>

<h2>Delete</h2>
<form method="POST">
    <label for="id">Enter ID:</label>
    <input type="number" id="id" name="id">
    <button type="submit" name="delete">Delete</button>
</form>

</body>
</html>